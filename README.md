<h1> 🤖 Poll bot
<img src="images/bmo.png"
       align="right" width="200" height="177" />
</h1>

> Bot inspired by [matrix-poll-bot](https://github.com/babolivier/matrix-poll-bot).

Example:

<img src="images/output-poll.png"  width="441" height="550">


# 📢 Commands

- `!help`: shows this message.
- `!m on`: enables multiple options (only the first user's vote will count).
- `!m off`: disables multiple options.
- `!poll`: creates a poll with the given question and options.

# 🎉 Things to improve / Future features

- [ ] **Multiple votes handling**: instead of storing the votes in an array as users vote, find out how to get the reactions of a message and do the counting with that. 
- [ ] **Command to close the poll** (is it needed?)
- [ ] Get output format consistency between Element's Android app and the Desktop app.
- [ ] Change the login process to be done with [nio](https://github.com/poljar/matrix-nio), instead of [simplematrixbotlib](https://github.com/KrazyKirby99999/simple-matrix-bot-lib).
- [ ] **Get it to work with encrypted chats**.
