import json
from dataclasses import dataclass, field
import simplematrixbotlib as botlib
import re
import random
import nio
import markdown
from mautrix.types import (EventType, ReactionEvent)
import json
import os

#--------------- GLOBAL VARIABLES --------------#
CONFIG_FILE = "auth.json"
PREFIX = '!'
bot_address = "" # full bot name
poll_info = {} # keys: even_it and question
votes = {} # keys: the emojis that at least have 1 vote, values: number of votes that received that emoji
voters = [] # users that have voted
multiple = False

# Emojis
emojis = []
# [Octopus(1), Ghost(2), Robot(3), Pig(4), Pinguin(5), Unicorn(6), Racoon(7), Sunflower(8), Dinosaur (9), Bomb(10), Pride Flag(11), Watermelon (12)]
REACTIONS = ["\U0001F419", "\U0001F47B", "\U0001F916", "\U0001F437", "\U0001F427", "\U0001F984", "\U0001F99D", "\U0001F33B", "\U0001F996", "\U0001F4A3", "\U0001F308", "\U0001F349"]
EMOJI_REGEX = r"^[\u2600-\u26FF\u2700-\u27BF\U0001F300-\U0001F5FF\U0001F600-\U0001F64F\U0001F680-\U0001F6FF\U0001F900-\U0001F9FF]"
#-----------------------------------------------#

#--------------- LOGIN -------------------------#
# Get current folder (relative path)
current_dir = os.getcwd().split("/")
current_dir = current_dir[len(current_dir)-1]

# If current_dir is not the repository's folder, change path to repository
if current_dir != "element-poll-bot":
    os.chdir("element-poll-bot")

# Open and read CONFIG_FILE
with open(CONFIG_FILE) as f:
    data = json.load(f)

    # Store bot complete name
    bot_address = data["bot_address"]

    # Prepare credentials
    creds = botlib.Creds(homeserver=data["server"],
                        username=data["username"], 
                        access_token=data["access_token"])

bot = botlib.Bot(creds)
#------------------------------------------------#

@bot.listener.on_message_event
async def help_message(room, message):
    """
    (!help) Displays the help message.

    Parameteres
    -----------
    None
    """
    match = botlib.MessageMatch(room, message, bot, PREFIX)

    if not (match.is_not_from_this_bot() and match.prefix() and match.command("help")):
        return

    # Prepare message to send
    msg = f'''<h1>Help</h1><br><p><strong>What is this bot?</strong> I can create a polls!</p><br><p>Commands:</p><br><ul><li><code>{PREFIX}help</code>: shows this message</li><li><code>{PREFIX}m on</code>: it allows to choose multiple options</li><li><code>{PREFIX}m off</code>: it does not allow to choose multiple options</li><li><code>{PREFIX}poll <QUESTION> <OPTION1> <OPTION2></code>: creates a poll with the given question and options.</li><ul><li>Example:<br><pre><code>{PREFIX}poll What do we have for dinner?<br><br>Pizza<br>Vegan lasagna<br>Miso soup<br></code></pre><br></li></ul><li><code>{PREFIX}r</code>: shows the results of the poll.</li></ul>'''
    content = {
      "msgtype": "m.text",
      "body": msg,
       "format": "org.matrix.custom.html",
       "formatted_body": msg
    }

    await bot.async_client.room_send(room.room_id, "m.room.message", content)

@bot.listener.on_message_event
async def multiple_message(room, message):
    """
    (!m) Enable/disable choosing multiple options in a poll.

    Parameteres
    -----------
    on : enable multiple choices
    off : disable multiple choices
    """
    match = botlib.MessageMatch(room, message, bot, PREFIX)

    if not (match.is_not_from_this_bot() and match.prefix() and match.command("m")):
        return

    # Global variables
    global multiple

    if len(match.args()) == 1: # If there is ONE command
        if match.args()[0] == "on":# If command "on"

            # Enable multiple options
            multiple = True

            msg = "Multiple options allowed!"
        elif match.args()[0] == "off": # If command "off"

            # Disable multiple options
            multiple = False

            msg = "Multiple options are NOT allowed!"
    else: # If no command or more than one command have been provided
        msg = "Command is incompleted. Type `!help` to review the available commands."

    # Prepare message to send
    content = {
        "msgtype": "m.text",
        "body": msg,
        "format": "org.matrix.custom.html",
        "formatted_body": markdown.markdown(msg)
    }

    await bot.async_client.room_send(room.room_id, "m.room.message", content)

@bot.listener.on_message_event
async def poll_command(room, event):
    """
    (!poll) Creates a poll if a question and several options arer provided.

    Parameteres
    -----------
    question: the title of the poll.
    question(s): the several options to choose from separated by a line break.
    """
    match = botlib.MessageMatch(room, event, bot, PREFIX)

    # Global variables
    global emojis

    # Re-initialise emojis array
    emojis = []

    if match.is_not_from_this_bot() and match.prefix() and match.command("poll"):

        text_array = []

        # Get each arguments as an element of an array (text_array)
        for line in str(event.body).splitlines():
            if line: # if line is not empty

                # Remove command at the begining (!poll)
                if line.split()[0] == f"{PREFIX}poll":

                    # Remove first element of array
                    line = line.split()[1:]
                    line = " ".join(line)

                # Get all message lines without command
                text_array.append(line)

        # Get questions
        question = text_array[0]

        # Keep poll question
        poll_info["question"] = question

        # Get available choices
        choices = text_array[1:]

        if len(choices) <= 1:  # If there is less than 2 options
            response = "You need to enter at least 2 choices."

            # Prepare message to send to room
            content = {
                "msgtype": "m.text",
                "body": response
            }

            await bot.async_client.room_send(room.room_id, "m.room.message", content)

            return

        # Initialise variable
        choices_final = []

        r = re.compile(EMOJI_REGEX)

        # Generate an array of random emojis
        random_emojis = random.sample(REACTIONS, len(choices))

        for choice in choices:
            # Remove white spaces at the end and start of the string
            choice_tmp = choice.strip()

            # Check if there is an emoji at the begining of option
            user_emoji = r.search(choice_tmp[0])

            if user_emoji:  # If choice has an emoji at the beginning

                # Get the emoji at the begining of option
                emoji = choice_tmp[0]

            else:
                # Get an emoji
                emoji = random_emojis.pop()

                # Insert emoji at the begining
                choice_tmp = emoji + " " + choice_tmp              
 
            # Update final array of options
            choices_final.append(choice_tmp)

            # Update emojis array
            emojis.append(emoji)

        # Create a string of choices_final array
        choices_show = "<br>".join(choices_final)

        # Prepare message in HTML format
        msg = f'''<b>New poll from <a href="https://matrix.to/#/{event.sender}">{event.sender}</a>!</b><br><br>
                Question: <i><u>{question}</u></i><br>
                <br>Choices:<br>
                {choices_show}'''

        content = {
                "msgtype": "m.text",
                "body": msg,
                "format": "org.matrix.custom.html",
                "formatted_body": msg
            }

        # Send poll
        resp = await bot.async_client.room_send(room.room_id, "m.room.message", content)

        # Keep the event_id of the formatted poll sent by bot
        poll_info["event_id"] = resp.event_id
        
        # Prepare message in HTML format
        message = "**Note**: by default, polls don't allow multiple choices. Send `!m on` to allow multiple choices!"
        content = {
            "msgtype": "m.text",
            "body": message,
            "format": "org.matrix.custom.html",
            "formatted_body": markdown.markdown(message)
        }

        await bot.async_client.room_send(room.room_id, "m.room.message", content)

        # Add emoji reactitons to poll
        for emoji in emojis:
            content = {
                "m.relates_to": {
                    "rel_type": "m.annotation",
                    "event_id": resp.event_id,
                    "key": emoji
                }
            }

            await bot.async_client.room_send(room.room_id, "m.reaction", content)

@bot.listener.on_custom_event(nio.UnknownEvent)
async def get_reaction(room, event):
    """
    Listenes to reaction events so it can get the vote(s) of the users.
    """

    if event.type == "m.reaction" and event.sender != bot_address:

        # If it is not allowed to choose multiple options
        if not multiple:
            if (event.sender in voters):
                print(f"{event.sender} has already voted!")

                # Prepare message in HTML format
                msg = f'<a href="https://matrix.to/#/{event.sender}">{event.sender}</a> has already voted!<br><i>If you want to enable multiple choices, send "!m on".</i>'

                content = {
                    "msgtype": "m.text",
                    "body": msg,
                    "format": "org.matrix.custom.html",
                    "formatted_body": markdown.markdown(msg)
                }
                await bot.async_client.room_send(room.room_id, "m.room.message", content)
                return

        # Get the reaction the user has added to the poll
        user_reaction = event.source["content"]["m.relates_to"]["key"]

        if user_reaction in votes: # If someone has already voted for this option (the emoji is already in votes dict)
            votes[user_reaction] = votes.get(user_reaction) + 1 # add one vote to this option
        else: # If this is the first time someone has voted por this option
            votes[user_reaction] = 1 # initialise with one vote

        # Update voters array with the user that just voted
        voters.append(event.sender)

@bot.listener.on_message_event
async def results_message(room, event):
    """
    (!r) Display the results of the poll and re-initialises poll variables.

    Parameteres
    -----------
    None
    """
    match = botlib.MessageMatch(room, event, bot, PREFIX)

    if not (match.is_not_from_this_bot() and match.prefix() and match.command("r")):
        return

    # Global variables
    global votes
    global voters
    global emojis
    global multiple

    # Initialization of variables
    winner_option = ""
    winner_votes = 0
    votes_rendered = []
    total_voted = len(votes.keys())

    if votes:
        # Get the most voted
        for option in votes: # Iterate keys (emojis) of votes dict

            # Update the array with "emoji : percentage % of votes"
            votes_rendered.append(f"{option}: {round(votes.get(option) / total_voted * 100)}%")

            # Calculate winner
            if winner_votes < votes.get(option):

                # Update winner
                winner_votes = votes.get(option)
                winner_option = option

        # Creat an string of voted_rendered
        votes_rendered = '<br>'.join(votes_rendered)

        # Get poll name
        poll_name = poll_info.get("question")

        # Prepare message to send to room
        message = f"Results of <i><u>{poll_name}</u></i> is:<br>{votes_render}<br><br>Winner is {winner_option}!"
    else:
        message = "There are no votes."

    # Re-initialise arrays for the next poll
    votes = {}
    voters = []
    emojis = []
    multiple = False

    # Get poll info
    poll_event_id = poll_info.get("event_id")
    resp = await bot.async_client.room_get_event(room.room_id, poll_event)

    # Remove bot name at the beginning of event returned by room_get_event
    temp = str(resp.event).split()
    poll_body = " ".join(temp[1:])

    # Prepare message to send to room
    message_formatted = f'<mx-reply><blockquote><a href="https://matrix.to/#/{room.room_id}/{poll_event_id}?via=matrix.org">Poll</a>:<br>{poll_body}</blockquote></mx-reply>{message}'
    content = {
        "body": message,
        "format": "org.matrix.custom.html",
        "formatted_body": message_formatted,
        "msgtype": "m.text",
        "m.relates_to": {
            "m.in_reply_to": {
                "event_id": poll_info.get("event_id")
      }
     }
    }

    await bot.async_client.room_send(room.room_id, "m.room.message", content)

bot.run()
